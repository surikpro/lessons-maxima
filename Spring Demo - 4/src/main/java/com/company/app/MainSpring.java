package com.company.app;

import com.company.SignUpService;
import com.company.config.ApplicationConfig;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class MainSpring {
    public static void main(String[] args) {
        ApplicationContext applicationContext = new AnnotationConfigApplicationContext(ApplicationConfig.class);
        SignUpService service = applicationContext.getBean(SignUpService.class);
        service.signUp("simple666@dimple.com", "!!!bSSDSFHDFSF&*&" +
                "" +
                "");
    }
}
