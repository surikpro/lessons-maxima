package com.company.validators.password;

import org.springframework.stereotype.Component;

@Component
public class PasswordCharactersValidatorImpl implements PasswordValidator {
    @Override
    public boolean isValid(String password) {
        if (!(password.indexOf('&') != -1
                && password.indexOf('!') != -1
                && password.indexOf('*') != -1)) {
            System.err.println("Отсутствуют спец символы");
            return false;
        } else return true;
    }
}
