package com.company.validators;

public interface SignUpDataValidator {
    boolean isValid(String email, String password);
}
