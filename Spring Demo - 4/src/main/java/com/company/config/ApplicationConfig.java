package com.company.config;

import com.company.SignUpService;
import com.company.blacklist.PasswordBlackList;
import com.company.blacklist.PasswordBlackListFileImpl;
import com.company.blacklist.PasswordBlackListHardcodeImpl;
import com.company.repositories.AccountsRepository;
import com.company.repositories.AccountsRepositoryNamedParameterJdbcTemplateImpl;
import com.company.validators.SignUpDataValidator;
import com.company.validators.SignUpDataValidatorEmailAndPasswordImpl;
import com.company.validators.email.EmailValidator;
import com.company.validators.email.EmailValidatorRegexImpl;
import com.company.validators.password.PasswordByLengthValidator;
import com.company.validators.password.PasswordCharactersValidatorImpl;
import com.company.validators.password.PasswordValidator;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@PropertySource(value = "classpath:application.properties")
@ComponentScan(value = "com.company")
@Component
public class ApplicationConfig {


    private final Environment environment;
    @Autowired
    public ApplicationConfig(Environment environment) {
        this.environment = environment;
    }


    @Bean
    public DataSource dataSource(HikariConfig hikariConfig) {
        return new HikariDataSource(hikariConfig);
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty("db.url"));
        config.setDriverClassName(environment.getProperty("db.driverClassName"));
        config.setUsername(environment.getProperty("db.user"));
        config.setPassword(environment.getProperty("db.password"));
        config.setMaximumPoolSize(environment.getProperty("db.hikari.max-pool-size", Integer.class));
        return config;
    }
}
