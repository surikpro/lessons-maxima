package com.company.app;

import com.company.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

public class MainQuery {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        Query<Student> query = session.createQuery("select s from Student s where "
                + "s.lastName = 'Zaripov'", Student.class);

        Student student = query.getSingleResult();
        int i = 0;

        session.close();

    }
}
