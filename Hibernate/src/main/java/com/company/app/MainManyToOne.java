package com.company.app;

import com.company.models.Course;
import com.company.models.Lesson;
import com.company.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class MainManyToOne {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();



        Session session = sessionFactory.openSession();

        Student student = Student.builder()
                .firstName("Aydar")
                .lastName("Zakirov")
                .courses(new ArrayList<>())
                .build();

        session.save(student);

        Course java = Course.builder()
                .title("Java")
                .build();
        session.save(java);

        Course sql = Course.builder()
                .title("SQL")
                .build();
        session.save(sql);

        Lesson javaCore = Lesson.builder()
                .course(java)
                .name("Java core")
                .build();

        Lesson spring = Lesson.builder()
                .course(java)
                .name("Spring")
                .build();

        Lesson selects = Lesson.builder()
                .course(sql)
                .name("Select")
                .build();

        Lesson inserts = Lesson.builder()
                .course(sql)
                .name("Inserts")
                .build();

        session.save(javaCore);
        session.save(spring);
        session.save(selects);
        session.save(inserts);

        session.save(java);
        session.save(sql);

        Student existedStudent = session.load(Student.class, 1L);

        existedStudent.getCourses().add(sql);
        existedStudent.getCourses().add(java);
        session.save(existedStudent);


        session.close();
        sessionFactory.close();
    }
}
