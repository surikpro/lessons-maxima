package com.company.app;

import com.company.models.Course;
import com.company.models.Lesson;
import com.company.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;

public class MainManyToMany {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        session.beginTransaction();

        Student student1 = Student.builder()
                .firstName("Aydar")
                .lastName("Zakirov")
                .courses(new ArrayList<>())
                .build();

        session.save(student1);

        Student student2 = Student.builder()
                .firstName("Ayrat")
                .lastName("Zaripov")
                .courses(new ArrayList<>())
                .build();

        session.save(student2);

        Course java = Course.builder()
                .title("Java")
                .build();

        Course sql = Course.builder()
                .title("SQL")
                .build();

        session.save(java);
        session.save(sql);

        student1.getCourses().add(sql);
        student2.getCourses().add(java);

        session.save(student1);
        session.save(student2);
        session.getTransaction().commit();
        session.close();
        sessionFactory.close();
    }
}
