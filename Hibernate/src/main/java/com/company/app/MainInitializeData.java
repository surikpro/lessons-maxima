package com.company.app;

import com.company.models.Course;
import com.company.models.Lesson;
import com.company.models.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;

public class MainInitializeData {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session session = sessionFactory.openSession();

        session.beginTransaction();


        Student student1 = Student.builder()
                .firstName("Aydar")
                .lastName("Zakirov")
                .courses(new ArrayList<>())
                .build();

        Student student2 = Student.builder()
                .firstName("Ayrat")
                .lastName("Zaripov")
                .courses(new ArrayList<>())
                .build();

        Course java = Course.builder()
                .title("Java")
                .build();

        Course sql = Course.builder()
                .title("SQL")
                .build();

        Lesson selects = Lesson.builder()
                .course(sql)
                .name("Select")
                .build();

        Lesson inserts = Lesson.builder()
                .course(sql)
                .name("Inserts")
                .build();

        Lesson javaCore = Lesson.builder()
                .course(java)
                .name("Java core")
                .build();

        Lesson spring = Lesson.builder()
                .course(java)
                .name("Spring")
                .build();

        session.save(selects);
        session.save(inserts);
        session.save(javaCore);
        session.save(spring);

        session.save(java);
        session.save(sql);

        student1.getCourses().add(sql);
        student1.getCourses().add(java);
        student2.getCourses().add(sql);
        student2.getCourses().add(java);

        session.save(student1);
        session.save(student2);

        session.getTransaction().commit();
        session.close();

        session = sessionFactory.openSession();
        session.close();
    }
}
