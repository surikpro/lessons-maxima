package com.company.config;

import com.company.SignUpService;
import com.company.blacklist.PasswordBlackList;
import com.company.blacklist.PasswordBlackListFileImpl;
import com.company.blacklist.PasswordBlackListHardcodeImpl;
import com.company.repositories.AccountsRepository;
import com.company.repositories.AccountsRepositoryNamedParameterJdbcTemplateImpl;
import com.company.validators.SignUpDataValidator;
import com.company.validators.SignUpDataValidatorEmailAndPasswordImpl;
import com.company.validators.email.EmailValidator;
import com.company.validators.email.EmailValidatorRegexImpl;
import com.company.validators.password.PasswordByLengthValidator;
import com.company.validators.password.PasswordCharactersValidatorImpl;
import com.company.validators.password.PasswordValidator;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

import javax.sql.DataSource;

@PropertySource(value = "classpath:application.properties")
public class ApplicationConfig {


    private final Environment environment;
    @Autowired
    public ApplicationConfig(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public SignUpService signUpService() {
        return new SignUpService(accountsRepository(), passwordBlackListFile(), signUpDataValidator());
    }

    @Bean
    public PasswordBlackList passwordBlackListFile() {
        return new PasswordBlackListFileImpl(environment.getProperty("passwordBlackListFile.fileName"));
    }

    @Bean
    public PasswordBlackList passwordBlackListHardcode() {
        return new PasswordBlackListHardcodeImpl();
    }

    @Bean
    public SignUpDataValidator signUpDataValidator() {
        return new SignUpDataValidatorEmailAndPasswordImpl(emailValidatorRegex(), passwordValidatorByCharacters());
    }

    @Bean
    public EmailValidator emailValidatorRegex() {
        EmailValidatorRegexImpl emailValidatorRegex = new EmailValidatorRegexImpl();
        emailValidatorRegex.setRegex(environment.getProperty("emailValidatorRegex.regex"));
        return emailValidatorRegex;
    }

    @Bean
    public PasswordValidator passwordValidatorByCharacters() {
        return new PasswordCharactersValidatorImpl();
    }

    @Bean
    public PasswordValidator passwordValidatorByLength() {
        PasswordByLengthValidator validator = new PasswordByLengthValidator();
        validator.setMinLength(environment.getProperty("passwordValidatorByLength.min-length", Integer.class));
        return validator;
    }

    @Bean
    public AccountsRepository accountsRepository() {
        return new AccountsRepositoryNamedParameterJdbcTemplateImpl(dataSource());
    }

    @Bean
    public DataSource dataSource() {
        return new HikariDataSource(hikariConfig());
    }

    @Bean
    public HikariConfig hikariConfig() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(environment.getProperty("db.url"));
        config.setDriverClassName(environment.getProperty("db.driverClassName"));
        config.setUsername(environment.getProperty("db.user"));
        config.setPassword(environment.getProperty("db.password"));
        config.setMaximumPoolSize(environment.getProperty("db.hikari.max-pool-size", Integer.class));
        return config;
    }
}
