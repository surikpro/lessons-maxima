package com.company;

import com.company.blacklist.PasswordBlackList;
import com.company.models.Account;
import com.company.repositories.AccountsRepository;
import com.company.validators.SignUpDataValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class SignUpService {
    private final PasswordBlackList passwordBlackList;
    private final SignUpDataValidator validator;
    private final AccountsRepository accountsRepository;
    @Autowired
    public SignUpService(AccountsRepository accountsRepository, @Qualifier(value = "passwordBlackListFile") PasswordBlackList passwordBlackList, SignUpDataValidator validator) {
        this.accountsRepository = accountsRepository;
        this.validator = validator;
        this.passwordBlackList = passwordBlackList;
    }

    public void signUp(String email, String password) {
        if (!validator.isValid(email, password)) {
           return;
        }
        if (passwordBlackList.contains(password)) {
            return;
        }
        System.out.println("Регистрация прошла успешно!");

        Account account = Account.builder()
                .email(email)
                .password(password)
                .build();
        accountsRepository.save(account);
    }
}
