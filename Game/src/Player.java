public class Player {
    // удар, при этом если есть оружие,
    // то меняется урон. Можно использовать Proxy

    private String name;
    private int health;
    private Weapon weapon;

    public Player(String name, int health) {
        this.name = name;
        this.health = health;
    }

    public int hit() {
        return health;
    }
}
