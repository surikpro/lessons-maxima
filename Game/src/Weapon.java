public class Weapon {
    // по разному может наносить урон,
    // урон, определяется алгоритмом урона
    // можно использовать паттерн Стратегию
    private String weaponName;
    private int damage;

    public Weapon(String weaponName) {
        this.weaponName = weaponName;
    }
}
