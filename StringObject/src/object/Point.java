package object;

public class Point {
    private  int x;
    private int y;
    private int z;

    public Point(int x, int y, int z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getZ() {
        return z;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Point)) {
            return false;
        }
        if (this == object) {
            return true;
        }

        Point that = (Point) object;
        return this.x == that.x && this.y == that.y && this.z == that.z;
    }
// так делать очень плохо, не делаем так;
//    public boolean equals(Point that) {
//        if (that == null) {
//            return false;
//        }
//        if (this == that) {
//            return true;
//        }
//        return this.x == that.x && this.y == that.y && this.z == that.z;
//    }
}
