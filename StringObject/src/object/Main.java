package object;

public class Main {
    public static void main(String[] args) {
        Point a = new Point(5, 7,9);
        Point b = new Point(5, 7,9);
        Point d = new Point(5, 7,9);
        Point f = new Point(5, 7,9);

        Point[] points = {a, b, d, f};
        System.out.println("EqualsUtil - " + EqualsUtil.isEquals(points));

        System.out.println(a == b);
        System.out.println(a.equals(b));
        Point c = null;
        System.out.println(a.equals(c));
        System.out.println(a.equals(a));
    }
}
