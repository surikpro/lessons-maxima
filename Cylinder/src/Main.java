public class Main {
    public static void main(String[] args) {
        Circle circle = new Circle(3.75);
        System.out.println(circle.getRadius());
        System.out.println(String.format("%.2f", circle.getArea()));
        Cylinder cylinder = new Cylinder(5.55, 7.25);
        System.out.println(cylinder.getRadius());
        System.out.println(cylinder.getHeight());
        System.out.println(String.format("%.2f", cylinder.getArea()));
        System.out.println(String.format("%.3f", cylinder.getVolume()));
    }
}
