package com.company.validators.password;

public interface PasswordValidator {
    boolean isValid(String password);
}
