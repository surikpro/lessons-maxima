package com.company.validators;

import com.company.validators.email.EmailValidator;
import com.company.validators.password.PasswordValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class SignUpDataValidatorEmailAndPasswordImpl implements SignUpDataValidator {

    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;
    @Autowired
    public SignUpDataValidatorEmailAndPasswordImpl(EmailValidator emailValidator, @Qualifier(value = "passwordValidatorByLength") PasswordValidator passwordValidator) {
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    @Override
    public boolean isValid(String email, String password) {
        return emailValidator.isValid(email) && passwordValidator.isValid(password);
    }
}
