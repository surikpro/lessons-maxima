package com.company.validators.email;

public interface EmailValidator {
    boolean isValid(String email);
}
