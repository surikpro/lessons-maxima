package com.company.blacklist;

public interface PasswordBlackList {
    public boolean contains(String password);
}
