package devices;

public class DeviceOutputErrImpl implements DeviceOutput {
    @Override
    public String getInformation() {
        return "Реализация на основе err-потока";
    }

    @Override
    public void write(String message) {
        System.err.println(message);
    }
}
