package devices;

import java.time.LocalTime;

public class DeviceInputWithTime implements DeviceInput {
    private DeviceInput deviceInput;

    public DeviceInputWithTime(DeviceInput deviceInput) {
        this.deviceInput = deviceInput;
    }

    @Override
    public String getInformation() {
        return "Реализация входного потока со временем на базе " + deviceInput.getInformation();
    }

    @Override
    public String read() {
        return LocalTime.now() + ": " + deviceInput.read();
    }
}
