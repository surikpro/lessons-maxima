package devices;

public interface DeviceOutput extends Device {
    void write(String message);
}
