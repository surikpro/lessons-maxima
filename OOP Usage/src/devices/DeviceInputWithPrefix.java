package devices;

public class DeviceInputWithPrefix implements DeviceInput {
    private DeviceInput deviceInput;
    private String prefix;

    public DeviceInputWithPrefix(String prefix, DeviceInput deviceInput) {
        this.deviceInput = deviceInput;
        this.prefix = prefix;
    }

    @Override
    public String getInformation() {
        return "Реализация входного потока с префиксом";
    }

    @Override
    public String read() {
        return prefix + ": " + deviceInput.read();
    }
}
