package devices;

public interface Device {
    String getInformation();
}
