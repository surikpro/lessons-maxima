package devices;

public interface DeviceInput extends Device {
    String read();
}
