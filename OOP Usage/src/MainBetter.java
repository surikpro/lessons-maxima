import devices.*;
import devices.impl.IOServiceImpl;

public class MainBetter {
    public static void main(String[] args) {
        IOService service = new IOServiceImpl();

        DeviceInput scanner = new DeviceInputScanner();
        DeviceInput withPrefix = new DeviceInputWithPrefix("ОТ ПОЛЬЗОВАТЕЛЯ", scanner);
        DeviceInput withTime = new DeviceInputWithTime(withPrefix);
        DeviceInput withAnotherPrefix = new DeviceInputWithPrefix("ПОЛУЧЕНИЕ СООБЩЕНИЕ В ", withTime);

        DeviceOutput deviceOutput = new DeviceOutputStandardImpl();

        service.setInput(withAnotherPrefix);
        service.setOutput(deviceOutput);
        service.printInformationAboutDevices();

        while(true) {
            String message = service.read();
            service.write(message);
        }
    }
}
