import devices.DeviceInput;
import devices.DeviceInputScanner;
import devices.DeviceInputWithPrefix;
import devices.DeviceInputWithTime;

public class MainDevices {
    public static void main(String[] args) {
        DeviceInput scanner = new DeviceInputScanner();
        DeviceInput withPrefix = new DeviceInputWithPrefix("ОТ ПОЛЬЗОВАТЕЛЯ", scanner);
        DeviceInput withTime = new DeviceInputWithTime(withPrefix);
        DeviceInput withAnotherPrefix = new DeviceInputWithPrefix("ПОЛУЧЕНИЕ СООБЩЕНИЕ В ", withTime);
        String message = withAnotherPrefix.read();
        System.out.println(message);

    }
}
