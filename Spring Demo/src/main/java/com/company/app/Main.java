package com.company.app;

import com.company.blacklist.PasswordBlackList;
import com.company.blacklist.PasswordBlackListFileImpl;
import com.company.SignUpService;
import com.company.validators.email.EmailValidatorRegexImpl;
import com.company.validators.password.PasswordCharactersValidatorImpl;
import com.company.validators.SignUpDataValidator;
import com.company.validators.SignUpDataValidatorEmailAndPasswordImpl;

public class Main {
    public static void main(String[] args) {
//        PasswordBlackList passwordBlackList = new PasswordBlackListHardcodeImpl();
        PasswordBlackList passwordBlackList = new PasswordBlackListFileImpl("bad_passwords.txt");
        EmailValidatorRegexImpl emailValidator = new EmailValidatorRegexImpl();
        emailValidator.setRegex(".+@.+");
//        PasswordByLengthValidator passwordValidator = new PasswordByLengthValidator();
//        passwordValidator.setMinLength(8);
        PasswordCharactersValidatorImpl passwordValidator = new PasswordCharactersValidatorImpl();
        SignUpDataValidator validator = new SignUpDataValidatorEmailAndPasswordImpl(emailValidator, passwordValidator);
        SignUpService signUpService = new SignUpService(null, passwordBlackList, validator);
        signUpService.signUp("zakirov@gmail.com", "simple_dimple");
    }
}
