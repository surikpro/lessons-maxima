package com.company.validators;

import com.company.validators.email.EmailValidator;
import com.company.validators.password.PasswordValidator;

public class SignUpDataValidatorEmailAndPasswordImpl implements SignUpDataValidator {

    private final EmailValidator emailValidator;
    private final PasswordValidator passwordValidator;

    public SignUpDataValidatorEmailAndPasswordImpl(EmailValidator emailValidator, PasswordValidator passwordValidator) {
        this.emailValidator = emailValidator;
        this.passwordValidator = passwordValidator;
    }

    @Override
    public boolean isValid(String email, String password) {
        return emailValidator.isValid(email) && passwordValidator.isValid(password);
    }
}
