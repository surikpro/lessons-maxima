public class FlourPacker {
    public static void main(String[] args) {
        System.out.println(canPack(3, 4, 30));
    }

    public static boolean canPack(int bigCount, int smallCount, int goal) {
        if ((goal < 0) || (smallCount < 0) || (bigCount < 0)) {
            return false;
        } else if (bigCount*5 + smallCount < goal) {
            return false;
        }
        return goal % 5 <= smallCount;
    }
}
