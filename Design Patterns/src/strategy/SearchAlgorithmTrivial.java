package strategy;

public class SearchAlgorithmTrivial implements SearchAlgorithm {
    private Sequence sequence;

    public SearchAlgorithmTrivial(Sequence sequence) {
        this.sequence = sequence;
    }

    @Override
    public boolean search(int element) {
        for (int i = 0; i < sequence.toArray().length; i++) {
            if (sequence.toArray()[i] == element) {
                return true;
            }
        }
        return false;
    }
}
