package strategy;

public class Main {
    public static void main(String[] args) {
        int array[] = {4, 77, 10, 100, 5, 6, 8};
        Sequence sequence = new SequenceArrayImpl(array);
        System.out.println(sequence.isExists(1000));
    }
}
