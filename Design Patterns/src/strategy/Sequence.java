package strategy;

public interface Sequence {
    boolean isExists(int element);
    int[] toArray();
}
