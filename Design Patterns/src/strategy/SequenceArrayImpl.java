package strategy;

public class SequenceArrayImpl implements  Sequence {
    private static final int THRESHOLD = 5;
    private int[] sequence;
    private SearchAlgorithm searchAlgorithm;

    public SequenceArrayImpl(int[] sequence) {
        this.sequence = sequence;
        if (sequence.length < THRESHOLD) {
            this.searchAlgorithm = new SearchAlgorithmTrivial(this);
        } else {
            this.searchAlgorithm = new BinarySearch(this);
        }
    }
    @Override
    public boolean isExists(int element) {
        return searchAlgorithm.search(element);
    }

    @Override
    public int[] toArray() {
        return sequence;
    }
}
