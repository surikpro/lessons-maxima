package strategy;

public interface SearchAlgorithm {
    boolean search(int element);
}
