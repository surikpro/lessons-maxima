package observer.lite;



public class Main {
    public static void main(String[] args) {
        Button exit = new ExitButton();
        exit.onClick(() -> System.exit(255));

        exit.click();
    }
}
