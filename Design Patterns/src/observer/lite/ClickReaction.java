package observer.lite;

public interface ClickReaction {
    void handle();
}
