package observer.hard;

public class DocumentsUtilImpl implements DocumentsUtil {
    private static final int MAX_PROCESSORS_COUNT = 5;
    private DocumentProcessor[] processors;
    private int processorsCount = 0;

    public DocumentsUtilImpl() {
        this.processors = new DocumentProcessor[MAX_PROCESSORS_COUNT];
    }

    @Override
    public void addDocument(String document) {
        System.out.println("Получен документ - " + document);
        notifyObservers(document);
    }

    @Override
    public void addProcessor(DocumentProcessor processor) {
        if(processorsCount < MAX_PROCESSORS_COUNT) {
            processors[processorsCount] = processor;
            processorsCount++;
        } else {
            System.err.println("Превышено максимальное количество обрадотчиков");
        }
    }

    @Override
    public void notifyObservers(String document) {
        for (int i = 0; i < processorsCount; i++) {
            processors[i].handleDocument(document);
        }
    }
}
