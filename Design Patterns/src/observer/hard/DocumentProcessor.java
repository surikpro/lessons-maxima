package observer.hard;

public interface DocumentProcessor {
    void handleDocument(String document);
}
