package observer.hard;

public interface DocumentsUtil {
    void addDocument(String document);
    void addProcessor(DocumentProcessor processor);

    void notifyObservers(String document);

}
