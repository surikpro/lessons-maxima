package observer.hard;

public class CertificateProcessor implements DocumentProcessor {
    @Override
    public void handleDocument(String document) {
        if (document.contains("Справка")) {
            System.out.println("Обнаружена справка, отправлена на печать.");
        }
    }
}
