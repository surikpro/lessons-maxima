package observer.hard;

public class StatementsProcessor implements DocumentProcessor {
    @Override
    public void handleDocument(String document) {
        if (document.contains("Заявление")) {
            System.out.println("Обнаружено заявление. Направлено в соответствующий отдел");
        }
    }
}
