package factorymethod;

import java.time.LocalDate;
import java.time.LocalTime;

public class CertificatesGenerator implements DocumentsGenerator {
    @Override
    public Document generate(String text) {
        return new Certificate(" <" + text + "> от " + LocalDate.now(), text);
    }
}
