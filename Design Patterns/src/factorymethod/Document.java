package factorymethod;

public interface Document {
    String getText();
    String getTitle();
}
