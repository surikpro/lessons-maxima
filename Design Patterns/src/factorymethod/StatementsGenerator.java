package factorymethod;

import java.time.LocalDate;

public class StatementsGenerator implements DocumentsGenerator {
    @Override
    public Document generate(String text) {
        return new Statement(" <" + text + "> от " + LocalDate.now(), text);
    }
}
