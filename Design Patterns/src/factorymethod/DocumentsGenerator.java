package factorymethod;

public interface DocumentsGenerator {
    Document generate(String text);
}
