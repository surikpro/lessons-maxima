package proxy.hard;

public interface After {
    void after();
}
