package proxy.hard;

public interface MailService {
    void sendMessage(String email, String text);
}
