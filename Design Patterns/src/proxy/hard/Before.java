package proxy.hard;

public interface Before {
    void before();
}
