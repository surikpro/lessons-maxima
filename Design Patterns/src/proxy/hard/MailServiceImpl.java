package proxy.hard;

import java.time.LocalTime;

public class MailServiceImpl implements MailService {
    @Override
    public void sendMessage(String email, String text) {
        System.out.println("На " + email + " было отправлено сообщение - " + text + " в " + LocalTime.now());
    }
}
