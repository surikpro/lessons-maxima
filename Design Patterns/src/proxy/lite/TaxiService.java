package proxy.lite;

public class TaxiService {
    public void setOnline(Driver[] drivers) {
        for (int i = 0; i < drivers.length; i++) {
            drivers[i].drive();
        }
    }
}
