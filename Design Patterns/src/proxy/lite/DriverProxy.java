package proxy.lite;

public class DriverProxy extends Driver {
    private Driver driver;
    private After after;
    private Instead instead;
    private Before before;


    public DriverProxy(Driver driver) {
        super(driver.getName());
        this.driver = driver;
    }

    @Override
    public void drive() {
        if (before != null) {
            before.before();
        }

        if (instead != null) {
            instead.instead();
        } else {
            driver.drive();
        }

        if (after != null) {
            after.after();
        }
    }

    public void setAfter(After after) {
        this.after = after;
    }

    public void setInstead(Instead instead) {
        this.instead = instead;
    }

    public void setBefore(Before before) {
        this.before = before;
    }
}
