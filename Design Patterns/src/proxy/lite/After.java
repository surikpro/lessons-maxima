package proxy.lite;

public interface After {
    void after();
}
