package proxy.lite;

public interface Before {
    void before();
}
