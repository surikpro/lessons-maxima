package proxy.lite;

public class Main {
    public static void main(String[] args) {
        Driver marsel = new Driver("Марсель");

        Driver airat = new Driver("Айрат");

        DriverProxy airatProxy = new DriverProxy(airat);
        airatProxy.setBefore(() -> System.out.println("Айрат, пожалуйста аккуратнее езжай!"));
        airatProxy.setInstead(() -> System.out.println("Нет бензина"));
        airatProxy.setAfter(() -> System.out.println("Ура, мы приехали!"));

        Driver victor = new Driver("Виктор");

        TaxiService service = new TaxiService();
        service.setOnline(new Driver[]{marsel, airatProxy, victor});
    }
}
