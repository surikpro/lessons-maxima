
import java.util.*;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int n = Integer.parseInt(scanner.nextLine());
        int m = Integer.parseInt(scanner.nextLine());
        List<Long> cn = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            cn.add(scanner.nextLong());
        }

        Collections.sort(cn, Collections.reverseOrder());

        cn = cn.subList(0, Math.min(m, cn.size()));

        if (cn.get(0) - cn.get(cn.size() - 1) == 0) {
            System.out.println(cn.get(0));
        }

        Long account = cn.get(0);
        Long counter = 0;


        Long left = cn.get(0);
        Long right = cn.get(cn.size() - 1);
        Long mid;
        while (right - left > 1) {
            Long sum = 0L;
            mid = (right + left) / 2;
            for (int i = 0; i < n; i++) {
                sum += cn.get(i) / mid;
            }
            if (sum < m) {
                right = mid;
            } else {
                left = mid;
            }
        }
        System.out.println(left);
    }
}

//        Arrays.sort(data);
//        Long left = data[0];
//        Long right = data[data.size() - 1];
//        Long mid = 0L;
//        while (right - left > 1) {
//            Long sum = 0L;
//            mid = (right + left) / 2;
//            for (int i = 0; i < numberOfAccounts; i++) {
//                sum += data.get(i) / mid;
//            }
//            if (sum < managerCount) {
//                right = mid;
//            } else {
//                left = mid;
//            }
//            System.out.println(left);


//        data.sort(Comparator.reverseOrder());
//        int counter;
//
//        for (int i = 1; i <= managerCount; i++) {
//            int x = numberOfAccounts  / i;
//            counter = i;
//
//            for (int j = 1; j < data.size(); j++) {
//                int y = Math.toIntExact(data.get(j));
//
//                while (true) {
//                    if (counter == managerCount) {
//                        System.out.println(x);
//                        return;
//                    }
//                    if ((y = y - x) < 0) {
//                        break;
//                    } else {
//                        counter++;
//                    }
//                }
//            }
//        }


//