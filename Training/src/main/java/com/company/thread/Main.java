package com.company.thread;

import java.sql.SQLOutput;
import java.util.Set;
import java.util.TreeSet;

public class Main {
    public static void main(String[] args) {
        Set<Number> set = new TreeSet<>();
        set.add(5);
        set.add(5L);
        set.add(5.0);
        System.out.println(set.size());
        Printer printer = new Printer();
        Printer printer1 = new Printer();
        Thread childThread = new Thread(printer);
        Thread childThread1 = new Thread(printer1);

        childThread.start();
        childThread1.start();
        for (int i = 0; i < 5; i++) {
            System.out.println(i);
        }
    }
}

class Printer implements Runnable {
    @Override
    public void run() {
        System.out.println("I'm printer");
        System.out.println("Printer thread finished");
    }
}
