package com.company.bubble_sort;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        int[] array = new int[] {8, 9, 2, 30, 1, 56, 30, 89, 76, 1, 90};
        System.out.println(Arrays.toString(bubbleSort(array)));

    }
    public static int[] bubbleSort(int[] arrayOfNumbers) {
        for (int i = 0; i < arrayOfNumbers.length - 1; i++) {
            for (int j = 1; j < arrayOfNumbers.length - i; j++) {
                if (arrayOfNumbers[j - 1] > arrayOfNumbers[j]) {
                    int temp = arrayOfNumbers[j - 1];
                    arrayOfNumbers[j - 1] = arrayOfNumbers[j];
                    arrayOfNumbers[j] = temp;
                }
            }
        }
        return arrayOfNumbers;
    }
}
