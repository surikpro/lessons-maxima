package com.company.primitives;

import java.sql.SQLOutput;

public class Main {
    public static void main(String[] args) {
        int a = 5;
        int b = 10;
        System.out.println("A = " + a + ". B = " + b);
        int c = a;
        a = b;
        b = c;
        System.out.println(a + " " + b);


    }
}
