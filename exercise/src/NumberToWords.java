public class NumberToWords {
    public static void main(String[] args) {
        numberToWords(100);
        System.out.println(getDigitCount(2345644));
    }
    public static void numberToWords(int number) {
        if (number < 0) {
            System.out.println("Invalid Value");
        } else {
            int reversed = reverse(number);
            String stringNumber = "";
            for (int i = 0; i < getDigitCount(number); i++) {
                int remainder = reversed % 10;
                reversed /= 10;
                switch (remainder) {
                    case 0: stringNumber = "Zero";
                        break;
                    case 1: stringNumber = "One";
                        break;
                    case 2: stringNumber = "Two";
                        break;
                    case 3: stringNumber = "Three";
                        break;
                    case 4: stringNumber = "Four";
                        break;
                    case 5: stringNumber = "Five";
                        break;
                    case 6: stringNumber = "Six";
                        break;
                    case 7: stringNumber = "Seven";
                        break;
                    case 8: stringNumber = "Eight";
                        break;
                    case 9: stringNumber = "Nine";
                        break;
                }
                System.out.println(stringNumber);
            }
        }
    }

    public static int reverse(int number) {
        int reversed = 0;
        while (number != 0) {
            int digit = number % 10;
            reversed = reversed * 10 + digit;
            number /= 10;
        }
        return reversed;
    }
    public static int getDigitCount(int number) {
        int digitCount = 1;
        if (number >= 0) {
            while (number > 9) {
                int remainder = number % 10;
                number /= 10;
                digitCount++;
            }
        } else {
            return -1;
        }
        return digitCount;
    }
}
