package com.company.my_demo.repository;

import com.company.my_demo.model.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;
import java.util.Optional;

public interface TodoRepository extends JpaRepository<Todo, Long>{
	List<Todo> findByUserName(String user);

	Todo save(Todo todo);

	void delete(Todo todo);

	Optional<Todo> findById(long id);
}
