package com.company.todo.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.company.todo.model.Todo;

public interface TodoRepository extends JpaRepository<Todo, Long>{
	List<Todo> findByUserName(String user);

	Todo save(Todo todo);

	void delete(Todo todo);

	Optional<Todo> findById(long id);
}
