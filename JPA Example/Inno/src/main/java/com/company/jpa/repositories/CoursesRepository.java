package com.company.jpa.repositories;

import com.company.jpa.models.Course;

public interface CoursesRepository {
    void save(Course course);
}
