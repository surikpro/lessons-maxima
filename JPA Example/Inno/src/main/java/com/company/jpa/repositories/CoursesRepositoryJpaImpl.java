package com.company.jpa.repositories;

import com.company.jpa.models.Course;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

public class CoursesRepositoryJpaImpl implements CoursesRepository {
    private EntityManager entityManager;

    public CoursesRepositoryJpaImpl(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public void save(Course course) {
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        entityManager.persist(course);
        transaction.commit();
    }
}
