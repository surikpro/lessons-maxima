import com.company.jpa.models.Course;
import com.company.jpa.repositories.CoursesRepository;
import com.company.jpa.repositories.CoursesRepositoryJpaImpl;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import javax.persistence.EntityManager;

public class Main {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate\\hibernate.cfg.xml");

        SessionFactory sessionFactory = configuration.buildSessionFactory();

        EntityManager entityManager = sessionFactory.createEntityManager();

        CoursesRepository coursesRepository = new CoursesRepositoryJpaImpl(entityManager);

        Course java = Course.builder()
                .title("Java")
                .build();

        Course sql = Course.builder()
                .title("SQL")
                .build();

        coursesRepository.save(java);
        coursesRepository.save(sql);
    }
}

