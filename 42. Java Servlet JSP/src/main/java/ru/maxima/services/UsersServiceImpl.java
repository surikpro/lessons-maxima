package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.maxima.exceptions.IncorrectEmailOrPasswordException;
import ru.maxima.dto.AccountDto;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.List;

import static ru.maxima.dto.AccountDto.from;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class UsersServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UsersServiceImpl(AccountsRepository accountsRepository, PasswordEncoder passwordEncoder) {
        this.accountsRepository = accountsRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void signUp(String email, String password) {
        Account account = Account.builder()
                .email(email)
                .hash_password(passwordEncoder.encode(password))
                .build();

        accountsRepository.save(account);
    }

    @Override
    public void signIn(String email, String password) {
        Account account = accountsRepository.findByEmail(email).orElseThrow(IncorrectEmailOrPasswordException::new);

        if (!passwordEncoder.matches(password, account.getHash_password())) {
            throw new IncorrectEmailOrPasswordException();
        }
    }

    @Override
    public List<AccountDto> getAllUsers() {
        return from(accountsRepository.findAll());
    }

    @Override
    public List<AccountDto> searchUserByEmail(String email) {
        return from(accountsRepository.searchByEmail(email));
    }
}
