<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<script>
    function searchUsers(email) {
        let request = new XMLHttpRequest();

        request.open('GET', '/searchByUsers?email=' + email, false);

        request.send();

        if (request.status !== 200) {
            alert("Ошибка!")
        } else {
            let html = '<tr>' +
                '<th>Id</th>' +
                '<th>Email</th>' +
                '</tr>';


            let response = JSON.parse(request.response);

            for (let i = 0; i < response['accounts'].length; i++) {
                html += '<tr>';
                html += '<td>' + response['accounts'][i]['id'] + '</td>';
                html += '<td>' + response['accounts'][i]['email'] + '</td>';
                html += '</tr>'
            }

            document.getElementById('account_table').innerHTML = html;
        }
    }
</script>
<body>
<h1 style="color: ${color}">Search page</h1>
<label for="email">Enter email for search</label>
<input id="email" name="email" placeholder="Email" onkeyup="searchUsers(document.getElementById('email').value)">
<br>
<table id="account_table">

</table>
</body>
</html>
