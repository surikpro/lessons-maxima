package ru.maxima;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ArgumentsSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

@DisplayNameGeneration(value = DisplayNameGenerator.ReplaceUnderscores.class)
@DisplayName(value = "NumbersUtil is working when")
public class NumbersUtilTest {
    private NumbersUtil numbersUtil;

    @BeforeEach
    public void setUp() {
        numbersUtil = new NumbersUtil();
    }
    @ParameterizedTest(name = "return <true> on {0}")
    @ValueSource(ints = {2, 3, 71, 113})
    public void on_prime_numbers_return_true(int primeNumber) {
        assertTrue(numbersUtil.isPrime(primeNumber));
    }
    @ParameterizedTest(name = "return <false> on {0}")
    @ValueSource(ints = {121, 169})
    public void on_square_numbers_return_false(int sqrNumber) {
        assertFalse(numbersUtil.isPrime(sqrNumber));
    }
    @ParameterizedTest(name = "return <false> on {0}")
    @ArgumentsSource(CompositeNumbersArgumentsProvider.class)
    public void on_composite_numbers_return_false(int number) {
        assertFalse(numbersUtil.isPrime(number));
    }

}
