package ru.maxima;

import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.ArgumentsProvider;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class CompositeNumbersArgumentsProvider implements ArgumentsProvider {
    private Random random = new Random();
    @Override
    public Stream<? extends Arguments> provideArguments(ExtensionContext extensionContext) throws Exception {
        List<Arguments> numbers = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            int first = random.nextInt(100) + 3;
            int second = random.nextInt(100) + 3;
            int compositeNumber = first * second;
            Arguments argument = Arguments.of(compositeNumber);
            numbers.add(argument);
        }
        return numbers.stream();
    }
}
