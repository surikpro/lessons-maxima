package ru.maxima;

public class Main {
    public static void main(String[] args) {
        NumbersUtil numbersUtil = new NumbersUtil();
        System.out.println(numbersUtil.isPrime(113));
        System.out.println(numbersUtil.isPrime(121));
        System.out.println(numbersUtil.isPrime(21));
        System.out.println(numbersUtil.isPrime(169));
        System.out.println(numbersUtil.isPrime(71));
    }
}
