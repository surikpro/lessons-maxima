public class Remote {
    private static Remote instance;
    private String model;

    public Remote() {
        this.model = model;
    }

    public static Remote getInstance() {
        return instance;
    }

    static  {
        instance = new Remote();
    }

    public String getModel() {
        return model;
    }
}
