<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.util.List" %>
<%@ page import="ru.maxima.dto.AccountDto" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>List of users - ${accounts.size()}
</h1>
<table>
    <th>Id</th>
    <th>Email</th>
    <c:forEach items="${accounts}" var="account">
        <tr>
            <td>${account.id}</td>
            <td>${account.email}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
