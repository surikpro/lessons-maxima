package ru.maxima.servlets;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.maxima.config.ApplicationConfig;
import ru.maxima.dto.AccountDto;
import ru.maxima.services.UsersService;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

/**
 * 20.09.2021
 * 41. Java Servlet Example
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@WebServlet("/users")
public class UsersServlet extends HttpServlet {

    private UsersService usersService;

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext servletContext = config.getServletContext();
        ApplicationContext applicationContext = (ApplicationContext) servletContext.getAttribute("springContext");
        this.usersService = applicationContext.getBean(UsersService.class);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       List<AccountDto> accounts = usersService.getAllUsers();
       request.setAttribute("accounts", accounts);
        request.getRequestDispatcher("/jsp/users.jsp").forward(request, response);


//        PrintWriter writer = response.getWriter();
//
//        writer.println("<h1>Email</h1>");
//        writer.println("<table>");
//        writer.println("    <tr>");
//        writer.println("        <th>" + "Email of user" + "</th>");
//        writer.println("    </tr>");
//        for (String email : usersService.getAllEmails()) {
//            writer.println("<tr>");
//            writer.println("    <td>" + email + "</td>");
//            writer.println("</tr>");
//        }
//        writer.println("</table>");
    }
}
