package ru.maxima.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.maxima.dto.AccountDto;
import ru.maxima.models.Account;
import ru.maxima.repositories.AccountsRepository;

import java.util.List;
import java.util.stream.Collectors;

import static ru.maxima.dto.AccountDto.from;

/**
 * 06.09.2021
 * 36. Spring Demo
 *
 * @author Sidikov Marsel (First Software Engineering Platform)
 * @version v1.0
 */
@Service
public class UsersServiceImpl implements UsersService {

    private final AccountsRepository accountsRepository;

    @Autowired
    public UsersServiceImpl(AccountsRepository accountsRepository) {
        this.accountsRepository = accountsRepository;
    }

    @Override
    public void signUp(String email, String password) {
        Account account = Account.builder()
                .email(email)
                .password(password)
                .build();

        accountsRepository.save(account);
    }

    @Override
    public List<String> getAllEmails() {
        // a -> a.method() -> A::method
        // a -> System.out.println(a) -> System.out::println
        // a -> x.method(a) -> x::method
        return accountsRepository.findAll().stream().map(Account::getEmail).collect(Collectors.toList());
    }

    @Override
    public List<AccountDto> getAllUsers() {
        return from(accountsRepository.findAll());
    }

    @Override
    public List<AccountDto> searchUserByEmail(String email) {
        return from(accountsRepository.searchByEmail(email));
    }
}
