package comparing;

import java.util.Arrays;
import java.util.Comparator;

public class Main {
    public static void main(String[] args) {
        Integer[] numbers = {23, 10, -34, 1000, 55, 29, 98, 43};
//        comparing.BubbleSort.sort(numbers);
//        System.out.println(Arrays.toString(numbers));
        User user0 = new User(40L, "Марсель", "Сидиков", 28, 1.85);
        User user1 = new User(40L, "Алия", "Мухутдинова", 20, 1.71);
        User user2 = new User(40L, "Айрат", "Мухутдинов", 25, 1.54);
        User user3 = new User(40L, "Даниил", "Вдовинов", 29, 1.65);
        User user4 = new User(40L, "Салават", "Забиров", 22, 1.67);
        User user5 = new User(40L, "Ильгам", "Хасанов", 21, 1.75);
        User user6 = new User(40L, "Артур", "Колычев", 20, 1.69);
        User users[] = {user0, user1, user2, user3, user4, user5, user6};
//        comparing.BubbleSort.sort(users);

//        comparing.BubbleSort.sort(users, new comparing.UserByHeightComparator());
        BubbleSort.sort(users, Comparator.comparingDouble(User::getHeight));
        System.out.println(Arrays.toString(users));
    }
}
