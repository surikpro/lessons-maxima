package streams;

import comparing.User;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Main4 {
    private static List<User> usersList() {
        User user0 = new User(40L, "Марсель", "Сидиков", 28, 1.85);
        User user1 = new User(70L, "Алия", "Мухутдинова", 20, 1.71);
        User user2 = new User(13L, "Айрат", "Мухутдинов", 25, 1.54);
        User user3 = new User(44L, "Даниил", "Вдовинов", 29, 1.65);
        User user4 = new User(1L, "Салават", "Забиров", 22, 1.67);
        User user5 = new User(15L, "Ильгам", "Хасанов", 21, 1.75);
        User user6 = new User(9L, "Артур", "Колычев", 20, 1.69);


        List<User> users = new ArrayList<>();
        users.add(user0);
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);
        users.add(user5);
        users.add(user6);
        return users;
    }
    public static void main(String[] args) {

        usersList()
                .stream()
                .filter(user -> user.getAge() > 22)
                .sorted(Comparator.comparingInt(User::getAge))
                .map(user -> user.getFirstName() + " " + user.getLastName())
                .forEach(System.out::println);

//        filteredStream.forEach(printUser);
//        sortedStream.forEach(printUser);
    }
}
