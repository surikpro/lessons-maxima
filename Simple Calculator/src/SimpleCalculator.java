public class SimpleCalculator {

    private double firstNumber;
    private double secondNumber;

    public SimpleCalculator() {
        this.firstNumber = firstNumber;
        this.secondNumber = secondNumber;
    }

    public double getFirstNumber() {
        return firstNumber;
    }

    public double getSecondNumber() {
        return secondNumber;
    }

    public void setFirstNumber(double firstNumber) {
        this.firstNumber = firstNumber;
    }

    public void setSecondNumber(double secondNumber) {
        this.secondNumber = secondNumber;
    }

    public double getAdditionResult() {
        double sum = firstNumber + secondNumber;
        return sum;
    }

    public double getSubtractionResult() {
        double subtract = firstNumber - secondNumber;
        return subtract;
    }

    public double getMultiplicationResult() {
        double multiple = firstNumber * secondNumber;
        return multiple;
    }

    public double getDivisionResult() {
        if (secondNumber == 0) {
            return 0;
        } else {
            double division = firstNumber/secondNumber;
            return division;
        }
    }
}
